use crate::{error::Error, tools::TwitchSite::*, twitch::TWITCH_BASE_URL};
use futures::StreamExt;
use futures::TryStreamExt;
use std::{
    fmt::Display,
    fs::{self, File},
    str, *,
};
pub mod convert {

    pub fn str_to_usize(string: &str) -> usize {
        string
            .trim()
            .parse::<usize>()
            .expect("Input needs to be a natural number.")
    }
}
pub enum TwitchSite {
    Sites(Vec<String>),
    Clips(Vec<String>),
    Popouts(Vec<String>),
    Videos(Vec<String>),
    Chats(Vec<String>),
    // GameDirectories(Vec<String>),
}
/* pub fn rand() -> Result<u32, std::io::Error> {
    let mut buf = [0; 1024];
    let mut file = File::open("/dev/urandom")?;
    file.read_exact(&mut buf)?;
    let num = buf.iter().fold(0, |acc, x| acc + *x as u32) % 90000 + 10000;
    Ok(num)
} */

pub fn read_config() -> Result<Config> {
    let mut dir = dirs::config_dir().ok_or(Error::NoConfigDir)?;
    dir.reserve_exact(2);
    dir.push("twitchrs");
    dir.push("config.json");
    let string = fs::read_to_string(dir)?;
    let out = serde_json::from_str::<Config>(&string)?;
    Ok(out)
}
use serde::{Deserialize, Serialize};

impl Display for Config {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "    Auth Token: {token:#?}
        UserName: {name:#?}
        Game Blacklist: {game:#?}
        Partial Game Blacklist: {partial_game:#?}
        Title Blacklist: {title:#?}
            ",
            token = self.access_token,
            name = self.user_name,
            game = self.blacklist_game,
            partial_game = self.blacklist_partial_game,
            title = self.blacklist_title
        )
    }
}

#[derive(Deserialize, Serialize, Default, Clone)]
pub struct Config {
    pub access_token: String,
    pub user_name: String,
    pub(crate) live_player: String,
    #[serde(default)]
    pub(crate) live_player_args: Vec<String>,
    pub(crate) vod_player: String,
    #[serde(default)]
    pub(crate) vod_player_args: Vec<String>,
    #[serde(default)]
    pub blacklist_game: Vec<String>,
    #[serde(default)]
    pub blacklist_partial_game: Vec<String>,
    #[serde(default)]
    pub blacklist_title: Vec<String>,
    #[serde(default)]
    pub blacklist_user: Vec<String>,
}
/* pub enum Node {
    //Authorization,
    UserName,
    //BlackListGame,
    //BlackListPartialGame,
    //BlackListTitle,
    //Print,
} */
/* pub fn make_config() -> Config {
    let access_token = prompt::str("Access Token");
    let user_name = prompt::str("User Name");
    let config = Config {
        access_token: Some(access_token),
        user_name: Some(user_name),
        blacklist_game: None,
        blacklist_partial_game: None,
        blacklist_title: None,
    };
    write_config(&config);
    config
} */
#[derive(PartialEq)]
pub enum OtherBlackList {
    Game,
    Title,
    PartialBlackListGame,
    User,
    Print,
}
impl Config {
    pub(crate) fn extend(&mut self, to: OtherBlackList, items: Vec<String>) -> Result<()> {
        match to {
            OtherBlackList::Game => {
                self.blacklist_game.extend(items);
            }
            OtherBlackList::Title => {
                self.blacklist_title.extend(items);
            }
            OtherBlackList::PartialBlackListGame => {
                self.blacklist_partial_game.extend(items);
            }
            OtherBlackList::User => {
                self.blacklist_user.extend(items);
            }
            _ => unimplemented!(),
        }
        write_config(self)?;
        Ok(())
    }
}
use crate::error::Result;
use std::io::prelude::*;
use tokio::process::Command;
fn write_config(config: &Config) -> Result<()> {
    let mut dir = dirs::config_dir().ok_or(Error::NoConfigDir)?;
    dir.push("twitchrs");
    dir.push("config.json");
    let mut file = File::create(&dir)?;
    let json = serde_json::to_string_pretty(config)?;
    file.write_all(json.as_bytes())?;
    Ok(())
}

/* pub fn populate_field(field: Node) -> Config {
    let mut config = read_config();
    match field {
        // Authorization => config.access_token = Some(prompt::str("Access Token")),
        UserName => config.user_name = prompt::str("User Name"),
        // BlackList => unimplemented!(),
    };
    write_config(&config);
    config
} */
impl TwitchSite {
    pub(crate) async fn open(self) -> Result<()> {
        let urls: Vec<String> = match self {
            Sites(val) => val
                .into_iter()
                .map(|site| format!("{}/{}", TWITCH_BASE_URL, site))
                .collect(),
            Clips(val) => val
                .into_iter()
                .map(|site| format!("https://clips.twitch.tv/{}", site))
                .collect(),
            Popouts(val) => val
                .into_iter()
                .map(|site| {
                    format!(
                        "https://player.twitch.tv/?channel={}&parent=twitch.tv",
                        site
                    )
                })
                .collect(),
            Videos(val) => val
                .into_iter()
                .map(|site| format!("{}/{}/videos", TWITCH_BASE_URL, site))
                .collect(),
            Chats(val) => val
                .into_iter()
                .map(|site| format!("{}/popout/{}/chat", TWITCH_BASE_URL, site))
                .collect(),
            /*
            GameDirectories(val) => val
                .into_iter()
                .map(|site| format!("{}/directory/game/{}", TWITCH_BASE_URL, site))
                .collect(),
            */
        };
        futures::stream::iter(urls)
            .map(|site| Command::new("xdg-open").arg(site).output())
            .buffer_unordered(usize::MAX)
            .filter(|i| future::ready(i.is_err()))
            .map_ok(|_| ())
            .next()
            .await
            .unwrap_or(Ok(()))?;
        Ok(())
    }
}
