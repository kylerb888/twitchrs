// Same as get_live_follow_info but update every minute and notify when a user goes live, as well as notifies you as to what game they're playing.
use crate::error::Result;
use crate::{
    error::Error,
    presentation::Message::{Chat, System},
    tools,
    vod::{self, ChunkMsg, Period::*, PresentedMsg, Video::*},
    RunAt::{self, *},
};
use chrono::{DateTime, FixedOffset};
use futures::prelude::*;
use irc::client::prelude::{Client, Command::PRIVMSG, Prefix::Nickname, *};
use std::ops::{Deref, DerefMut};
use tokio::{join, time::sleep};
async fn present_vod_chat<'a>(
    vod_chat_data: &ChunkMsg,
    start_time: DateTime<FixedOffset>,
    untimed: bool,
) -> PresentedMsg {
    for (val, iter) in vod_chat_data.msgs.iter().zip(1..) {
        let offset = val.time.signed_duration_since(start_time);
        println!(
            "|+{hours}:{minutes:02}:{seconds:02}| {name}: {message}",
            hours = offset.num_hours(),
            minutes = offset.num_minutes() % 60,
            seconds = offset.num_seconds() % 60,
            message = val.text,
            name = val.name
        );

        if vod_chat_data.msgs.len() != iter {
            let test = vod_chat_data.msgs[iter]
                .time
                .signed_duration_since(val.time)
                .to_std()
                .unwrap();
            if !untimed {
                sleep(test).await;
            }
        }
    }
    vod_chat_data.msgs[vod_chat_data.msgs.len() - 1].to_owned()
}
pub async fn vod_chat_replay(
    vod_id: &str,
    mut cursor: &RunAt,
    config: &tools::Config,
    untimed: bool,
) -> Result<()> {
    // Each chat segment is approximately ten seconds.
    let client = reqwest::Client::new();
    let start_time = vod::get_past_vods(
        vod::Videos(Box::pin(stream::iter(Some(Ok(Id(vod_id.to_string())))))),
        Num(1),
        &client,
        config,
    )
    .await?
    .remove(0)
    .created_at;
    let mut vod_chat_data = vod::vod_chat(vod_id, &client, cursor, config).await?;
    cursor = &vod_chat_data.after;
    while let Cursor(Some(_)) = cursor {
        let preload = vod::vod_chat(vod_id, &client, cursor, config);
        let data = present_vod_chat(&vod_chat_data, start_time, untimed);
        let (current, preloaded) = join!(data, preload);

        vod_chat_data = preloaded?;
        let test = vod_chat_data.msgs[0]
            .time
            .signed_duration_since(current.time)
            .to_std()
            .unwrap();
        sleep(test).await;
        cursor = &vod_chat_data.after;
    }
    Ok(())
}
enum Message {
    Chat(ChatMessage),
    System(String),
}
struct ChatMessage {
    username: String,
    message: String,
}
struct IrcUsers(Vec<String>);
impl Deref for IrcUsers {
    type Target = Vec<String>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl DerefMut for IrcUsers {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
pub(crate) fn users_into_irc_channels(users: &mut Vec<String>) {
    for user in users {
        user.insert(0, '#');
    }
}

pub async fn chat(mut users: Vec<String>) -> Result<()> {
    users_into_irc_channels(&mut users);
    let mut rng = rand::thread_rng();
    use rand::Rng;
    let config = Config {
        nickname: Some(format!("justinfan{}", &(rng.gen_range(10000..=99999)))),
        server: Some("irc.chat.twitch.tv".into()),
        port: Some(6697),
        channels: users,
        ..Config::default()
    };
    let mut client = Client::from_config(config).await?;
    client.identify()?;

    let stream = client.stream()?;
    stream
        .err_into()
        .try_for_each(|msg| async {
            let message = match msg.command {
                PRIVMSG(_, text) => match msg.prefix.map_or(Err(Error::MessageHasNoPrefix), Ok)? {
                    Nickname(name, _, _) => Chat(ChatMessage {
                        username: name,
                        message: text,
                    }),
                    _ => return Err(Error::MessageHasNoUserName.into()),
                },
                _ => System(msg.to_string()),
            };
            if let Chat(msg) = message {
                println!(
                    "{name}: {message}",
                    name = msg.username,
                    message = msg.message
                );
            }
            Result::<_>::Ok(())
        })
        .await?;
    Ok(())
}
pub mod prompt {
    use crate::error::Result;
    use crate::tools::convert::str_to_usize;
    use std::{cmp::Reverse, fmt::Display, io};

    pub fn bool(action: impl Display) -> bool {
        let mut input = String::new();
        println!("\nWould you like to {action}? (y/n)", action = &action);
        io::stdin()
            .read_line(&mut input)
            .expect("Failed to read line.");
        matches!(
            input.trim(),
            "y" | "yes" | "ya" | "ye" | "yah" | "yeh" | "si" | "please"
        )
    }
    pub fn multi_num(action: impl Display) -> Result<Vec<usize>> {
        let mut input = String::new();
        println!(
            "\nIf you would like to {action}, enter the number corresponding with the entry.",
            action = &action
        );
        io::stdin().read_line(&mut input)?;
        let mut out: Vec<usize> = input
            .split_whitespace()
            .map(|i| i.parse::<usize>().map(|i| i - 1).map_err(From::from))
            .collect::<Result<Vec<usize>>>()?;
        out.sort_unstable_by_key(|item| Reverse(*item));
        Ok(out)
    }
    pub fn num(action: impl Display) -> usize {
        let mut input = String::new();
        println!(
            "\nIf you would like to {action}, enter the number corresponding with the entry.",
            action = &action
        );
        io::stdin()
            .read_line(&mut input)
            .expect("Failed to read line.");
        str_to_usize(&input) - 1
    }
    /* pub fn str(action: &str) -> String {
        let mut input = String::new();
        println!("Please input your {}.", action);
        io::stdin()
            .read_line(&mut input)
            .expect("Failed to read line,");
        input.pop();
        input
    } */
}
