use crate::parse::parse_args;
use app::RunningApp;
use eframe::NativeOptions;
use vod::RunAt;
mod api_constants;
mod app;
mod cache;
#[path = "nodes/clips.rs"]
mod clips;
mod error;
mod fetch;
#[path = "nodes/follows.rs"]
mod follows;
#[path = "nodes/games.rs"]
mod games;
mod loader;
mod parse;
mod presentation;
#[path = "nodes/streams.rs"]
mod streams;
mod tools;
#[path = "nodes/vod.rs"]
mod vod;

use crate::loader::twitch;

#[macro_export]
macro_rules! uor {
    ($x:expr, $y:expr) => {{
        match $x {
            Some(val) => val,
            None => return $y,
        }
    }};
}
fn main() {
    let args = parse_args().expect("Failed to get arguments from command-line");
    if args.optargs.gui {
        eframe::run_native(Box::new(RunningApp::default()), NativeOptions::default());
    } else {
        tokio::runtime::Builder::new_current_thread()
            .enable_all()
            .build()
            .expect("Failed to spawn tokio runtime")
            .block_on(args.execute())
            .expect("Failed to run twitchrs");
    }

    /* parse_args()
    .expect("Failed to get arguments from command-line")
    .execute()
    .await
    .expect("Failed to run twitchrs"); */
}

trait FindFirstAndTake<T, S>
where
    T: PartialEq<S>,
    S: PartialEq<T>,
{
    fn find_first_and_take(&mut self, search: &[S]) -> Option<T>;
}

impl<T, S> FindFirstAndTake<T, S> for Vec<T>
where
    T: PartialEq<S>,
    S: PartialEq<T>,
{
    fn find_first_and_take(&mut self, search: &[S]) -> Option<T> {
        self.iter()
            .position(|item| search.iter().any(|list| item == list))
            .map(|idx| self.remove(idx))
    }
}
