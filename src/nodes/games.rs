use std::{
    collections::HashMap,
    iter::FromIterator,
    ops::{Deref, DerefMut},
    rc::Rc,
};

use futures::{stream, StreamExt, TryStreamExt};
use reqwest::Client;

use crate::{
    api_constants::GAMES,
    cache::{cache_read, cache_write},
    fetch::{generate_pair, request, PostOrGet::*},
    tools::Config,
};

use serde::Deserialize;

#[derive(Deserialize)]
pub struct GameList {
    pub name: Rc<String>,
    pub id: Rc<String>,
}
use crate::error::Result;

#[derive(Deserialize)]
pub struct GameListData {
    pub data: Vec<GameList>,
}

impl Extend<(Rc<String>, Rc<String>)> for GameNames {
    fn extend<T: IntoIterator<Item = (Rc<String>, Rc<String>)>>(&mut self, iter: T) {
        self.0.extend(iter)
    }
}
#[derive(Default)]
pub(crate) struct GameNames(HashMap<Rc<String>, Rc<String>>);
impl Deref for GameNames {
    type Target = HashMap<Rc<String>, Rc<String>>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl DerefMut for GameNames {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl FromIterator<(Rc<String>, Rc<String>)> for GameNames {
    fn from_iter<T: IntoIterator<Item = (Rc<String>, Rc<String>)>>(iter: T) -> Self {
        let mut c = GameNames::new();
        c.extend(iter);
        c
    }
}
impl GameNames {
    fn new() -> Self {
        GameNames(HashMap::new())
    }
    fn with_capacity(cap: usize) -> Self {
        GameNames(HashMap::with_capacity(cap))
    }
    pub async fn from_ids(ids: Vec<Rc<String>>, client: &Client, config: &Config) -> Result<Self> {
        let mut cache: HashMap<Rc<String>, Rc<String>> = cache_read("game_name").await?;
        let (dictionary, queue) = ids.iter().fold(
            (GameNames::with_capacity(ids.len()), Vec::new()),
            |(mut dictionary, mut queue), id| {
                if let Some(val) = cache.get(&**id) {
                    dictionary.insert(Rc::clone(id), Rc::clone(val));
                } else {
                    queue.push(id);
                }
                (dictionary, queue)
            },
        );
        let dictionary: GameNames = stream::iter(queue.chunks(100))
            .map(|batch| async move {
                let batch = batch.iter().map(|i| &****i);
                let out = request(
                    GAMES,
                    client,
                    &generate_pair("id", batch).collect::<Vec<_>>(),
                    Get,
                    config,
                )
                .await?
                .json::<GameListData>()
                .await?
                .data;
                Result::<_>::Ok(out)
            })
            .buffer_unordered(usize::MAX)
            .map_ok(|i| stream::iter(i).map(Result::<_>::Ok))
            .try_flatten()
            .map_ok(|GameList { name, id }| (id, name))
            .inspect_ok(|(name, id)| {
                cache.insert(Rc::clone(id), Rc::clone(name));
            })
            .chain(stream::iter(dictionary.0).map(Ok))
            .try_collect::<GameNames>()
            .await?;
        cache_write("game_name", &cache).await?;
        Ok(dictionary)
    }
}
