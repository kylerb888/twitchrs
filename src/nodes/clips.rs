use std::{
    fmt::{self, Display},
    iter::FromIterator,
    ops::{Deref, DerefMut},
    rc::Rc,
    sync::Arc,
};

use futures::{stream, StreamExt, TryStreamExt};
use reqwest::Client;

use crate::{
    api_constants::{broadcaster_id, CLIPS},
    fetch::{
        GenericIdProperties, UserIds,
        {request, GenericIdPropertiesData, PostOrGet::*, UserId},
    },
    tools::Config,
};
impl Extend<String> for Clips {
    fn extend<T: IntoIterator<Item = String>>(&mut self, iter: T) {
        self.0.extend(iter)
    }
}
#[derive(Default)]
pub struct Clips(pub Vec<String>);

impl Deref for Clips {
    type Target = Vec<String>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
use crate::error::Result;
impl Clips {
    fn new() -> Self {
        Clips(Vec::new())
    }
    pub async fn create(users: Vec<String>, client: Client, config: Arc<Config>) -> Result<Self> {
        let ids = UserIds::from_names(
            stream::iter(users).map(Ok),
            client.clone(),
            Arc::clone(&config),
        )
        .await?;

        let send = ids
            .0
            .map_ok(|i| (i, client.clone(), Arc::clone(&config)))
            .map_ok(|(UserId { id, .. }, client, config)| async move {
                let out = request(CLIPS, &client, &[broadcaster_id(&id)], Post, &config)
                    .await?
                    .json::<GenericIdPropertiesData>()
                    .await?
                    .data;
                Result::<_>::Ok(out)
                // println!("{}", res);
            })
            .try_buffer_unordered(usize::MAX)
            .map_ok(|i| stream::iter(i).map(Result::<_>::Ok))
            .try_flatten()
            .map_ok(|GenericIdProperties { id }| id)
            .try_collect()
            .await?;
        Ok(send)
    }
}

impl Display for Clips {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for clip in &self.0 {
            writeln!(f, "https://clips.twitch.tv/{}", clip)?
        }
        Ok(())
    }
}

impl DerefMut for Clips {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl FromIterator<String> for Clips {
    fn from_iter<I: IntoIterator<Item = String>>(iter: I) -> Self {
        let mut c = Clips::new();
        c.extend(iter);
        c
    }
}
