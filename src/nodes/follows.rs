use crate::{api_constants::*, error::Error::*, fetch::{Cursor, *, self, UserId, UserIds, request}, tools::Config};
use chrono::{DateTime, Utc};
use futures::{stream, StreamExt, TryStreamExt};
use reqwest::Client;
use serde::Deserialize;
use std::{rc::Rc, sync::Arc, *};
#[derive(Deserialize, Debug)]
pub struct FollowItem {
    pub to_login: Arc<String>,
    pub to_id: Rc<String>,
    pub followed_at: DateTime<Utc>,
}
#[derive(Deserialize, Debug)]
pub struct FollowListData {
    pub total: usize,
    pub pagination: Cursor,
    pub data: Vec<FollowItem>,
}
use crate::error::Result;

pub async fn follow_user(mut add: Vec<String>, client: Client, config: Arc<Config>, add_or_delete: PostOrGet) -> Result<()> {
    add.push(config.user_name.to_owned());
    let mut user_ids = UserIds::from_names(
        stream::iter(add).map(Ok),
        client.clone(),
        Arc::clone(&config),
    )
    .await?
    .try_collect::<Vec<UserId>>()
    .await?;

    let origin = UserIds::extract_host(&mut user_ids, &config).ok_or(LoginNotFound)?;
    let bor = &origin;

    let mut status = stream::iter(user_ids)
        .map(|i| (i, client.clone(), Arc::clone(&config)))
        .map(|(UserId { id, .. }, client, config)| async move {
            request(
                FOLLOWS,
                &client,
                &[(from_id(&bor.id)), to_id(&id)],
                add_or_delete,
                &config,
            )
            .await?;
            Ok(())
        })
        .buffer_unordered(usize::MAX)
        .filter(|i| future::ready(i.is_err()));
    let out = status.next().await.unwrap_or(Ok(()));
    status.for_each(|_| future::ready(())).await;
    out
}
