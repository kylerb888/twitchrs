use std::{cmp::Reverse, pin::Pin, rc::Rc};

use chrono::{DateTime, FixedOffset, Utc};
use futures::{Stream, TryStreamExt, stream};
use reqwest::Client;

use crate::{
    api_constants::VIDEOS,
    fetch::{request, Cursor, PostOrGet::*, TwitchNode::*, UserId, UserIds},
    tools::Config,
};
use serde::Deserialize;
use Period::*;

#[derive(Deserialize, Clone, Debug)]
pub struct VodProperties {
    pub user_name: String,
    pub title: String,
    pub id: String,
    pub duration: String,
    pub created_at: DateTime<FixedOffset>,
}

#[derive(Deserialize)]
pub struct VodData {
    pub data: Vec<VodProperties>,
    pub pagination: Cursor,
}

impl From<UserIds> for Videos {
    fn from(user_ids: UserIds) -> Self {
        let out = user_ids.map_ok(|UserId { id, .. }| {
            Video::UserId(Rc::try_unwrap(id).unwrap_or_else(|i| i.to_string()))
        });
        Videos(Box::pin(out))
    }
}

pub struct Videos(pub Pin<Box<dyn Stream<Item = Result<Video>>>>);

#[derive(Debug)]
pub enum Video {
    Id(String),
    UserId(String),
}
#[derive(Clone, Copy)]
pub enum Period {
    Num(usize),
    Time(i64),
}

impl<'a> Period {
    pub(crate) fn get_period(&self) -> usize {
        match self {
            Num(val) => *val,
            Time(val) => *val as usize,
        }
    }
}
pair!(id, user_id);
impl<'a> Video {
    fn get(&'a self) -> (&'a str, &'a str) {
        match self {
            Video::Id(val) => id(&**val),
            Video::UserId(val) => user_id(&**val),
        }
    }
}
use crate::error::Result;
use crate::pair;
pair!(limit);
// As specified in the API Reference, you can only look at VODs for one user at a time, so for this instance we'll reuse our client to speed things up and wrap these functions under a for loop.
pub async fn get_past_vods(
    users: Videos,
    period: Period,
    client: &Client,
    config: &Config,
) -> Result<Vec<VodProperties>> {
    let per = &*period.get_period().to_string();

    let mut out = users
        .0
        .map_ok(|user| async move {
            let out = request(VIDEOS, client, &[user.get(), limit(per)], Get, config)
                .await?
                .json::<VodData>()
                .await?
                .data;
            Result::<_>::Ok(out)
        })
        .try_buffer_unordered(usize::MAX)
        .try_concat()
        .await?;

    out.sort_by_key(|VodProperties { created_at, .. }| Reverse(*created_at));

    match period {
        Time(val) => {
            let offset = val * 86400;
            let current_time = Utc::now();
            let limit = out.iter().position(|VodProperties { created_at, .. }| {
                current_time
                    .signed_duration_since(*created_at)
                    .to_std()
                    .unwrap()
                    .as_secs()
                    > offset as u64
            });
            if let Some(num) = limit {
                out.truncate(num);
            }

            Ok(out)
        }
        Num(val) => {
            out.truncate(val);
            Ok(out)
        }
    }
}

#[derive(Deserialize)]
pub struct ChatComments {
    comments: Vec<ChatData>,
    _next: Option<String>,
}
#[derive(Deserialize)]
pub struct ChatData {
    pub commenter: ChatCommenter,
    pub message: ChatBody,
    pub created_at: String,
}
#[derive(Deserialize)]
pub struct ChatCommenter {
    pub display_name: String,
}
#[derive(Deserialize)]
pub struct ChatBody {
    pub body: String,
}
#[derive(Clone)]
pub struct PresentedMsg {
    pub name: String,
    pub text: String,
    pub time: DateTime<FixedOffset>,
}
#[derive(Clone)]
pub struct ChunkMsg {
    pub msgs: Vec<PresentedMsg>,
    pub after: RunAt,
}
#[derive(Clone)]
pub enum RunAt {
    Cursor(Option<String>),
    OffsetSecs(String),
}
pair!(cursor, content_offset_seconds);
impl<'a> RunAt {
    fn get(&'a self) -> Option<(&'a str, &'a str)> {
        match self {
            Self::Cursor(Some(val)) => Some(cursor(&**val)),
            Self::Cursor(None) => None,
            Self::OffsetSecs(val) => Some(content_offset_seconds(&**val)),
        }
    }
}
/* pub fn get_thumbnail(urls: Vec<String>, client: &Client) -> Vec<Vec<u32>> {
    urls.into_iter()
        .map(|url| {
            request(
                &url.replace("{width}", "1280").replace("{height}", "720"),
                client,
                &None::<u8>,
                Get,
            )
            .bytes()
            .unwrap()
            .into_iter()
            .map(Into::into)
            .collect()
        })
        .collect()
} */

pub async fn vod_chat(
    vod_id: &str,
    client: &Client,
    cursor: &RunAt,
    config: &Config,
) -> Result<ChunkMsg> {
    let res = request(
        &Comments(vod_id).to_string(),
        client,
        &[cursor.get()],
        Get,
        config,
    )
    .await?
    .json::<ChatComments>()
    .await?;

    let adjusted: Result<Vec<PresentedMsg>> = res
        .comments
        .into_iter()
        .map(
            |ChatData {
                 commenter: ChatCommenter { display_name },
                 message: ChatBody { body },
                 created_at,
             }| {
                Ok(PresentedMsg {
                    name: display_name,
                    text: body,
                    time: DateTime::parse_from_str(&created_at, "%+")?,
                })
            },
        )
        .collect();
    Ok(ChunkMsg {
        msgs: adjusted?,
        after: match res._next {
            Some(val) => RunAt::Cursor(Some(val)),
            None => RunAt::Cursor(None),
        },
    })
}
