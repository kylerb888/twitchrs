use crate::app::Render;
use crate::tools::Config;
use crate::{
    api_constants::*,
    fetch::{generate_pair, PostOrGet::*, *},
    games::GameNames,
    loader::clear,
    parse::Args,
    uor,
};
use crate::{error::Result, loader::notify};
use async_stream::try_stream;
use egui::Color32;
use egui::CtxRef;
use egui::Image;
use egui::TextureId;
use egui::Ui;
use epi::Frame;
use futures::{Stream, StreamExt, TryStreamExt};
use image::Rgba;
use reqwest::Client;
use serde::Deserialize;
use std::sync::Arc;
use std::{
    cmp::Reverse,
    fmt::{Display, Write},
    iter::FromIterator,
    ops::{Deref, DerefMut},
    pin::Pin,
    rc::Rc,
    *,
};
use tokio::runtime::Runtime;
impl PreprocessedLiveList {
    fn title_contains_blacklist(&self, config: &Config) -> bool {
        config
            .blacklist_title
            .iter()
            .any(|x| self.title.contains(x))
    }
    fn game_is_blacklisted(&self, config: &Config) -> bool {
        config.blacklist_game.contains(uor!(&self.game_name, false))
    }
    fn game_contains_blacklist(&self, config: &Config) -> bool {
        let game_name = uor!(&self.game_name, false);
        config
            .blacklist_partial_game
            .iter()
            .any(|x| game_name.contains(x))
    }
    fn user_is_blacklisted(&self, config: &Config) -> bool {
        config.blacklist_user.contains(&self.user_name)
    }
    fn user_may_be_bot(&self) -> bool {
        self.user_name.len() != 6
            && ["elon_musk", "elonmusk", "shroud"]
                .iter()
                .any(|user| self.user_name.contains(user))
    }
    fn stream_is_blacklisted(&self, config: &Config) -> bool {
        self.game_is_blacklisted(config)
            || self.game_contains_blacklist(config)
            || self.title_contains_blacklist(config)
            || self.user_may_be_bot()
            || self.user_is_blacklisted(config)
    }
}

impl fmt::Display for PreprocessedLiveList {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.game_name.as_ref().map(Rc::deref).map(String::deref) {
            None => write!(
                f,
                concat!(
                    "{title}\n",
                    "      {user} Streaming for {viewer_count} viewer"
                ),
                title = self.title.trim(),
                user = self.user_name,
                viewer_count = self.viewer_count
            ),
            Some("Just Chatting") => write!(
                f,
                concat!(
                    "{title}\n",
                    "       {user} Just Chatting with {viewer_count} viewer"
                ),
                title = self.title.trim(),
                user = self.user_name,
                viewer_count = self.viewer_count
            ),
            Some("Watch Parties") => write!(
                f,
                concat!(
                    "{title}\n",
                    "       {user} Watching a Movie with {viewer_count} viewer"
                ),
                title = self.title.trim(),
                user = self.user_name,
                viewer_count = self.viewer_count
            ),
            Some("Special Events") => write!(
                f,
                concat!(
                    "{title}\n",
                    "       {user} Hosting a Special Event for {viewer_count} viewer"
                ),
                title = self.title.trim(),
                user = self.user_name,
                viewer_count = self.viewer_count
            ),
            Some(other) => write!(
                f,
                concat!(
                    "{title}\n",
                    "       {user} playing {activity} for {viewer_count} viewer"
                ),
                title = self.title.trim(),
                user = self.user_name,
                activity = other,
                viewer_count = self.viewer_count
            ),
        }?;
        if self.viewer_count != 1 {
            f.write_char('s')?;
        }
        Ok(())
    }
}

#[derive(PartialEq, Clone)]

pub struct ImageData {
    dimensions: [u32; 2],
    pixels: Vec<Color32>,
    texture_id: Option<TextureId>,
}

#[derive(Deserialize, PartialEq, Clone, Default)]
pub struct PreprocessedLiveList {
    pub game_id: Rc<String>,
    pub thumbnail_url: String,
    #[serde(skip)]
    pub thumbnail: Option<ImageData>,
    pub game_name: Option<Rc<String>>,
    pub title: String,
    pub user_name: String,
    pub user_login: String,
    pub viewer_count: u32,
}

impl Render for PreprocessedLiveList {
    fn render(
        &mut self,
        ui: &mut Ui,
        ctx: &CtxRef,
        frame: &mut Frame,
        runtime: &Runtime,
        client: &Client,
        config: &Arc<Config>,
    ) {
        let Self {
            ref user_name,
            ref viewer_count,
            ref title,
            ref game_name,
            ref mut thumbnail,
            ..
        } = *self;
        let ImageData {
            dimensions: [width, height],
            ref mut texture_id,
            ref mut pixels,
            ..
        } = *thumbnail.as_mut().unwrap();
        let texture_id = *texture_id.get_or_insert_with(|| {
            frame
                .tex_allocator()
                .alloc_srgba_premultiplied((width as usize, height as usize), &mem::take(pixels))
        });
        ui.vertical_centered(|ui| {
            ui.image(texture_id, [width as f32, height as f32]);
            ui.heading(title);
            let print = if let Some(game) = game_name {
                format!("{} - {} viewers - {}", user_name, viewer_count, game)
            } else {
                format!("{} - {} viewers", user_name, viewer_count)
            };
            ui.label(print);
            // ui.separator();
        });
        ui.separator();
    }
}

impl IntoIterator for LiveList {
    type Item = PreprocessedLiveList;

    type IntoIter = vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}
impl Extend<PreprocessedLiveList> for LiveList {
    fn extend<T: IntoIterator<Item = PreprocessedLiveList>>(&mut self, iter: T) {
        self.0.extend(iter)
    }
}
impl Stream for LiveStream {
    type Item = Result<PreprocessedLiveList>;

    fn poll_next(
        mut self: Pin<&mut Self>,
        cx: &mut task::Context<'_>,
    ) -> task::Poll<Option<Self::Item>> {
        self.0.poll_next_unpin(cx)
    }
}
use image::GenericImageView;
pub struct LiveStream(pub Pin<Box<dyn Stream<Item = Result<PreprocessedLiveList>>>>);
impl LiveStream {
    pub fn with_thumbnails(self, client: Client) -> Self {
        let stream = self
            .map_ok(move |i| (i, client.clone()))
            .map_ok(|(mut item, client)| async move {
                let PreprocessedLiveList {
                    ref thumbnail_url,
                    ref mut thumbnail,
                    ..
                } = item;
                let res = client
                    .get(thumbnail_url
                        .replace("{width}", "640")
                        .replace("{height}", "360")
                    ).send()
                    .await?
                    .error_for_status()?
                    .bytes()
                    .await?;

                let image = image::load_from_memory(&res)?;

                let dimensions = [image.width(), image.height()];

                let pixels = image
                    .to_rgba8()
                    .pixels()
                    .map(|&Rgba([r, g, b, a])| Color32::from_rgba_premultiplied(r, g, b, a))
                    .collect();
                *thumbnail = Some(ImageData {
                    dimensions,
                    pixels,
                    texture_id: None,
                });
                Ok(item)
            })
            .try_buffer_unordered(usize::MAX)
            .boxed_local();
        Self(stream)
    }
    pub fn filtered(self, config: Arc<Config>, force: bool) -> Self {
        if force {
            return self;
        }
        Self(
            self.try_filter(move |i| future::ready(!i.stream_is_blacklisted(&config)))
                .boxed_local(),
        )
    }
    pub async fn collect_at_most(&mut self, num: usize) -> Result<LiveList> {
        self.take(num).try_collect().await
    }
    pub fn from_top_streams(client: Client, config: Arc<Config>) -> Self {
        let out = try_stream! {
            let mut pagination: Option<String> = None;
            loop {
                let query: Vec<_> = array::IntoIter::new([first("100"), language("en")]).chain(pagination.as_ref().map(String::as_str).map(after)).collect();
                let res = request(
                    STREAMS,
                    &client,
                    &query,
                    Get,
                    &config,
                )
                .await?
                .json::<PreprocessedLiveListData>()
                .await?;
                let out = LiveList(res.data).populate_games(&client, &config).await?;
                pagination = res.pagination.cursor;
                for item in out {
                    yield item;
                }

                if pagination == None { break; }
            }
        };
        LiveStream(Box::pin(out))
    }
    pub async fn into_live_list(self) -> Result<LiveList> {
        self.try_collect::<LiveList>().await
    }
    pub fn from_names(
        users: impl Stream<Item = Result<String>> + 'static,
        client: Client,
        config: Arc<Config>,
    ) -> Self {
        let out = users
            .chunks(100)
            .map(move |i| (i, client.clone(), Arc::clone(&config)))
            .map(|(batch, client, config)| async move {
                let batch = batch.into_iter().collect::<Result<Vec<_>>>()?;
                let out = request(
                    STREAMS,
                    &client,
                    &generate_pair("user_login", &batch).collect::<Vec<_>>(),
                    Get,
                    &config,
                )
                .await?
                .json::<PreprocessedLiveListData>()
                .await?
                .data;
                LiveList(out).populate_games(&client, &config).await
            })
            .buffer_unordered(usize::MAX)
            .map_ok(|i| futures::stream::iter(i).map(Ok))
            .try_flatten();
        LiveStream(Box::pin(out))
    }
}
#[derive(Default)]
pub struct LiveList(pub Vec<PreprocessedLiveList>);

impl Render for LiveList {
    fn render(
        &mut self,
        ui: &mut Ui,
        ctx: &CtxRef,
        frame: &mut Frame,
        runtime: &Runtime,
        client: &reqwest::Client,
        config: &Arc<Config>,
    ) {
        for entry in &mut self.0 {
            entry.render(ui, ctx, frame, runtime, client, config);
        }
    }
}

impl Deref for LiveList {
    type Target = Vec<PreprocessedLiveList>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl DerefMut for LiveList {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl Display for LiveList {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for (entry, num) in self.iter().zip(1..) {
            writeln!(f, "   {}. {}", num, entry)?;
        }
        Ok(())
    }
}

impl LiveList {
    pub(crate) fn update(&self, old_users: &[String]) {
        self.iter()
            .filter(|PreprocessedLiveList { user_name, .. }| !old_users.contains(&user_name))
            .for_each(
                |PreprocessedLiveList {
                     user_name,
                     game_name,
                     ..
                 }| {
                    let declaration = match game_name.as_ref().map(Rc::deref).map(String::deref) {
                        None => {
                            format!("{streamer} has gone live!", streamer = user_name)
                        }
                        Some("Just Chatting") => {
                            format!(
                                "{streamer} has gone live doing Just Chatting!",
                                streamer = &user_name,
                            )
                        }
                        Some("Watch Parties") => {
                            format!(
                                "{streamer} has gone live watching a movie!",
                                streamer = &user_name
                            )
                        }
                        Some("Special Events") => {
                            format!(
                                "{streamer} has gone live hosting a Special Event!",
                                streamer = &user_name
                            )
                        }
                        Some(other) => {
                            format!(
                                "{streamer} has gone live playing {game}!",
                                streamer = &user_name,
                                game = other
                            )
                        }
                    };
                    println!("    {}", declaration);
                    notify(&declaration);
                },
            );
    }
    pub(crate) fn total_viewership(&self) -> u32 {
        self.iter()
            .map(|PreprocessedLiveList { viewer_count, .. }| viewer_count)
            .sum()
    }
    // Print live follow summary.
    pub async fn from_followed(client: Client, args: &Args) -> Result<LiveList> {
        // let followed = api_get_followed_users(vec!(String::from("39123216")), 100);

        let config = &args.config;
        let target = config.user_name.to_owned();
        let logins = futures::stream::iter(Some(target));
        let list = UserIds::from_followed(logins, client.clone(), Arc::clone(&config))
            .await?
            .to_live_streams(client, Arc::clone(config))
            .filtered(Arc::clone(&args.config), args.optargs.force)
            .into_live_list()
            .await?
            .merged();
        // Clear the screen
        clear()?;
        println!("{}", list);
        let flattened = list.flatten();
        Ok(flattened)

        /*
        filtered_table.iter().zip(1..).for_each(|(entry, num)| {
            println!("    {}. {}", num, entry);
        });
        */
    }
    pub async fn from_games(
        games: impl Stream<Item = String>,
        client: Client,
        config: impl Into<Arc<Config>>,
    ) -> Result<LiveList> {
        let config = config.into();
        let game_ids = get_game_ids_from_names(games, client.clone(), Arc::clone(&config));

        let out = game_ids
            .map_ok(|(game, ..)| game)
            .chunks(100)
            .map(move |batch| (batch, client.clone(), Arc::clone(&config)))
            .map(|(batch, client, config)| async move {
                let batch = batch.into_iter().collect::<Result<Vec<_>>>()?;
                let adapted = batch.iter().map(Rc::deref);
                let query: Vec<_> = array::IntoIter::new([first("100"), language("en")])
                    .chain(generate_pair("game_id", adapted))
                    .collect();
                let res = request(STREAMS, &client, &query, Get, &config)
                    .await?
                    .json::<PreprocessedLiveListData>()
                    .await?
                    .data;
                LiveList(res).populate_games(&client, &config).await
            })
            .buffer_unordered(usize::MAX)
            .try_concat()
            .await?;
        Ok(out)
    }
    pub(crate) const fn new() -> LiveList {
        LiveList(Vec::new())
    }
    pub async fn populate_games(mut self, client: &Client, config: &Config) -> Result<Self> {
        // By default, getting streams from the Twitch API doesn't include the name of the games associated with the game ids. This function populates that field.
        let mut game_ids: Vec<Rc<String>> = self
            .iter()
            .map(|PreprocessedLiveList { game_id, .. }| Rc::clone(game_id))
            .collect();
        game_ids.sort(); // Sort and dedup elements in game_ids to ensure we're not sending the same game_ids multiple times.
        game_ids.dedup();
        let game_names = GameNames::from_ids(game_ids, client, config).await?;
        for PreprocessedLiveList {
            game_name, game_id, ..
        } in &mut self.0
        {
            *game_name = game_names.get(&*game_id).map(Rc::clone);
        }
        Ok(self)
    }
    pub fn merged(mut self) -> MergedLiveList {
        self.sort_by_key(|PreprocessedLiveList { game_id, .. }| Rc::clone(game_id));
        let mut out = Vec::with_capacity(self.len());
        while let Some(PreprocessedLiveList { game_id: first, .. }) = self.first() {
            let split = self
                .iter()
                .skip(1)
                .position(|PreprocessedLiveList { game_id, .. }| first != game_id);
            if let Some(split) = split {
                let segment: LiveList = self.drain(..=split).collect();

                out.push((segment.total_viewership(), segment));
            } else {
                self.shrink_to_fit();

                out.push((self.total_viewership(), self));

                break;
            }
        }
        out.shrink_to_fit();
        out.sort_by_key(|(viewership, ..)| Reverse(*viewership));

        out.into()
    }
}

impl From<Vec<(u32, LiveList)>> for MergedLiveList {
    fn from(i: Vec<(u32, LiveList)>) -> Self {
        MergedLiveList(i)
    }
}

pub struct MergedLiveList(Vec<(u32, LiveList)>);
impl Deref for MergedLiveList {
    type Target = Vec<(u32, LiveList)>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl DerefMut for MergedLiveList {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
impl Display for MergedLiveList {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut table = 0;
        println!();
        for (viewership, node) in &**self {
            if let [entry] = &***node {
                table += 1;
                writeln!(f, "  {}. {}", table, entry)?;
            } else {
                if let [PreprocessedLiveList {
                    game_name: Some(game),
                    ..
                }, ..] = &***node
                {
                    write!(f, "  •  {}:", game)?;
                } else {
                    f.write_str("  •  Unlisted:")?;
                }
                write!(f, " {} viewer", viewership)?;
                if *viewership != 1 {
                    f.write_str("s\n")
                } else {
                    f.write_char('\n')
                }?;

                for PreprocessedLiveList {
                    title,
                    user_name,
                    viewer_count,
                    ..
                } in &**node
                {
                    table += 1;
                    write!(
                        f,
                        concat!("       {}. {}\n", "           {} for {} viewer"),
                        table,
                        title.trim(),
                        user_name,
                        viewer_count
                    )?;
                    if *viewer_count != 1 {
                        writeln!(f, "s")
                    } else {
                        writeln!(f)
                    }?
                }
            };
        }
        Ok(())
    }
}
impl FromIterator<PreprocessedLiveList> for LiveList {
    fn from_iter<I: IntoIterator<Item = PreprocessedLiveList>>(iter: I) -> Self {
        let mut c = LiveList::new();
        c.extend(iter);
        c
    }
}
impl MergedLiveList {
    pub fn flatten(self) -> LiveList {
        self.0.into_iter().flat_map(|(_, i)| i).collect()
    }
}
#[derive(Deserialize)]
pub struct PreprocessedLiveListData {
    pub data: Vec<PreprocessedLiveList>,
    pub pagination: Cursor,
}

impl From<Vec<PreprocessedLiveList>> for LiveList {
    fn from(i: Vec<PreprocessedLiveList>) -> Self {
        LiveList(i)
    }
}
