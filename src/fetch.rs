pub(crate) fn generate_pair<'a, I>(
    key: &'a str,
    items: impl IntoIterator<Item = &'a I>,
) -> impl Iterator<Item = (&str, &str)>
where
    I: AsRef<str>,
    I: 'a + ?Sized,
{
    items.into_iter().map(move |x| (key, x.as_ref()))
}
use std::ops::Deref;
use crate::{api_constants::*, cache::cache_read, error::Error, follows::{FollowItem, FollowListData}, games::{GameList, GameListData}, loader::twitch, presentation::prompt, streams::{LiveStream, PreprocessedLiveList}, vod::{Period, VodProperties}};
use crate::{cache::cache_write, parse::StreamItem, tools::Config, vod::VodData};
use async_stream::try_stream;
use bytes::Bytes;
use chrono::{DateTime, Duration, Utc};
use time::UtcOffset;
use core::fmt::Display;
use futures::{future, stream, Stream, StreamExt, TryStreamExt};
use m3u8_rs::playlist::{ExtTag, MasterPlaylist, MediaPlaylist, MediaSegment, Playlist};
use reqwest::{header::*, Client, Response};
use serde::{Deserialize, Serialize};
use std::{
    cell::RefCell, cmp::Reverse, collections::HashMap, fmt, io, iter, mem, pin::Pin,
    process::Stdio, rc::Rc, sync::Arc, time::Instant,
};
use tokio::{
    io::{AsyncWrite, AsyncWriteExt},
    process::Command,
};
use unicase::UniCase;
use PostOrGet::*;
use TimeMetric::*;

pub(crate) enum TwitchNode<'a> {
    Comments(&'a str),
}
impl Display for TwitchNode<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Comments(id) => write!(f, "https://api.twitch.tv/v5/videos/{}/comments", id),
        }
    }
}
pub(crate) enum TimeMetric {
    Hours,
    Day,
    Days(u64),
    Week,
    Weeks(u64),
    Month,
    Months(u64),
    Year,
    Years(u64),
    Decade,
    Decades(u64),
}
impl Display for TimeMetric {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Hours => f.write_str("less than 1 day ago"),
            Day => f.write_str("1 day ago"),
            Days(days) => write!(f, "{} days ago", days),
            Week => f.write_str("1 week ago"),
            Weeks(weeks) => write!(f, "{} weeks ago", weeks),
            Month => f.write_str("1 month ago"),
            Months(months) => write!(f, "{} months ago", months),
            Year => f.write_str("1 year ago"),
            Years(years) => write!(f, "{} years ago", years),
            Decade => f.write_str("1 decade ago"),
            Decades(decades) => write!(f, "{} decades ago", decades),
        }
    }
}

#[derive(Debug)]
pub struct LiveProperties {
    pub user_name: String,
    pub live_status: Option<StreamItem>,
}

impl LiveProperties {
    pub async fn from_names(arg: Vec<String>, config: Arc<Config>) -> Result<Vec<LiveProperties>> {
        // We convert the user name responses to lowercase because .contains() is case-sensitive.
        let format = LiveStream::from_names(
            stream::iter(arg.clone().into_iter().map(Ok)),
            reqwest::Client::new(),
            config,
        )
        .into_live_list()
        .await?;
    
        let out: Vec<_> = arg
            .into_iter()
            .map(|mut stream| LiveProperties {
                live_status: format
                    .iter()
                    .find(|PreprocessedLiveList { user_name, .. }| {
                        UniCase::new(&stream) == UniCase::new(&user_name)
                    })
                    .map(
                        |PreprocessedLiveList {
                             game_name,
                             user_name,
                             ..
                         }| {
                            stream = user_name.to_owned();
                            StreamItem {
                                open_in_site: game_name.as_ref().map(Rc::deref).map(String::deref)
                                    == Some("Watch Parties"),
                            }
                        },
                    ),
                user_name: stream,
            })
            .collect();
        out.iter()
            .filter_map(|LiveProperties { live_status, user_name, .. }| live_status.is_none().then(|| user_name))
            .for_each(|user_name| eprintln!("{} is not live.", user_name));
        Ok(out)
    }
}

#[derive(Deserialize)]
pub struct UserName {
    pub user_name: String,
}

#[derive(Deserialize)]
pub struct GenericIdPropertiesData {
    pub data: Vec<GenericIdProperties>,
}
#[derive(Deserialize, Debug)]
pub struct Cursor {
    pub cursor: Option<String>,
}
impl Display for UserId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let Self { id, login, followed_at } = self;
        if let Some(followed_at) = followed_at {
            writeln!(f, "{}: {} - Following since {}", login, id, followed_at.naive_local())?
        } else {
            writeln!(f, "{}: {}", login, id)?
        }
        Ok(())
    }
}
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct UserId {
    pub login: Arc<String>,
    pub id: Rc<String>,
    #[serde(skip)]
    pub followed_at: Option<DateTime<Utc>>,
}
pair!(user_id);
impl UserId {
    fn get(&self) -> (&str, &str) {
        user_id(&**self.id)
    }
}
#[derive(Deserialize)]
pub struct UserIdData {
    pub data: Vec<UserId>,
}
use crate::error::Result;
pub fn get_game_ids_from_names(
    ids: impl Stream<Item = String>,
    client: Client,
    config: Arc<Config>,
) -> impl Stream<Item = Result<(Rc<String>, Rc<String>)>> {
    let out = ids
        .chunks(100)
        .map(move |i| (i, client.clone(), Arc::clone(&config)))
        .map(|(batch, client, config)| async move {
            let segment = request(
                GAMES,
                &client,
                &generate_pair("name", &batch).collect::<Vec<_>>(),
                Get,
                &config,
            )
            .await?
            .json::<GameListData>()
            .await?
            .data;
            Result::<_>::Ok(segment)
        })
        .buffer_unordered(usize::MAX)
        .map_ok(|c| stream::iter(c).map(Result::<_>::Ok))
        .try_flatten()
        .map_ok(|GameList { id, name }| (id, name));
    out
}
#[derive(Clone, Copy)]
pub enum PostOrGet {
    Post,
    Get,
    Delete,
}
pub(crate) async fn request<T: Serialize + ?Sized + std::fmt::Debug>(
    node: &str,
    client: &Client,
    query: &T,
    reqtype: PostOrGet,
    config: &Config,
) -> Result<Response> {
    let out = match reqtype {
        Get => client.get(node),
        Post => client.post(node),
        Delete => client.delete(node),
    }
    .header("Client-ID", "gp762nuuoqcoxypju8c569th9wz7q5")
    .header(AUTHORIZATION, &config.access_token) // NOIDONTTHINKSO
    .header(ACCEPT_ENCODING, "gzip")
    .query(query)
    .send()
    .await?
    .error_for_status()?;

    Ok(out)
}
impl Stream for UserIds {
    type Item = Result<UserId>;

    fn poll_next(
        mut self: Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Option<Self::Item>> {
        self.0.poll_next_unpin(cx)
    }
}
pub struct UserIds(pub Pin<Box<dyn Stream<Item = Result<UserId>>>>);
impl Stream for Vods {
    type Item = Result<Pin<Box<dyn Stream<Item = Result<VodProperties>>>>>;

    fn poll_next(
        mut self: Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Option<Self::Item>> {
        self.0.poll_next_unpin(cx)
    }
}
pub struct Vods(
    pub Pin<Box<dyn Stream<Item = Result<Pin<Box<dyn Stream<Item = Result<VodProperties>>>>>>>>,
);
impl Vods {
    pub(crate) async fn show(self, config: &Config) -> Result<()> {
        let mut res = self.processed().try_concat().await?;

        res.sort_by_key(|VodProperties { created_at, .. }| Reverse(*created_at));

        let res_length = res.len();
        let current_epoch = Utc::now();
        for (
            listnum,
            VodProperties {
                created_at,
                title,
                user_name,
                duration,
                ..
            },
        ) in (1..).zip(&res)
        {
            let time_string = {
                match current_epoch.signed_duration_since(*created_at).num_seconds() as u64 / 86400 {
                    0 => Hours,
                    1 => Day,
                    days @ 2..=6 => Days(days),
                    7..=13 => Week,
                    weeks @ 14..=29 => Weeks(weeks / 7),
                    30..=59 => Month,
                    months @ 60..=364 => Months(months / 30),
                    365..=729 => Year,
                    years @ 730..=3649 => Years(years / 365),
                    3650..=7299 => Decade,
                    decades => Decades(decades / 3650),
                }
            };
            println!(
                concat!(
                    "  {num}. {title}\n",
                    "{user} for {duration} on {started_at} ({ago})"
                ),
                num = listnum,
                title = title.trim(),
                user = user_name,
                duration = duration,
                started_at = created_at.format("%D @ %r"),
                ago = time_string
            );
        }
        match res_length {
            1 if prompt::bool("watch this VOD") => {
                twitch::load_vod(&res.remove(0usize).id, &config).await?
            }
            1 => {}
            0 => eprintln!("There are no vods to display."),
            _ => {
                twitch::load_vod(
                    &res.remove(prompt::num("watch one of these VODs")).id,
                    &config,
                )
                .await?
            }
        }
        Ok(())
    }
    pub(crate) fn processed(self) -> impl Stream<Item = Result<Vec<VodProperties>>> {
        self.map_ok(TryStreamExt::try_collect)
            .try_buffer_unordered(usize::MAX)
    }
    pub(crate) fn limit(self, limiter: Period) -> Self {
        let current_time = Utc::now();
        let out = match limiter {
            Period::Time(num) => self.limit_by_time(num, current_time),
            Period::Num(num) => self.limit_by_num(num),
        };
        Vods(out)
    }

    fn limit_by_num(
        self,
        num: usize,
    ) -> Pin<Box<dyn Stream<Item = Result<Pin<Box<dyn Stream<Item = Result<VodProperties>>>>>>>>
    {
        self.map_ok(move |i| i.take(num).boxed_local())
            .boxed_local()
    }

    fn limit_by_time(
        self,
        num: i64,
        current_time: DateTime<Utc>,
    ) -> Pin<Box<dyn Stream<Item = Result<Pin<Box<dyn Stream<Item = Result<VodProperties>>>>>>>>
    {
        self.map_ok(move |i| {
            i.try_take_while(move |VodProperties { created_at, .. }| future::ok(dbg!(*created_at + Duration::days(num)) > dbg!(current_time)))
            .boxed_local()
        })
        .boxed_local()
    }
}
use crate::pair;
impl UserIds {
    /// Note that this will try to receive all of the videos that a set of users have, paginating if needed. If you're looking to limit the amount of videos received, try .limit() or manually invoke .take() or .try_take_while()
    pub fn get_vods(self, client: Client, config: Arc<Config>) -> Vods {
        let out = self
            .map_ok(move |i| (i, Arc::clone(&config), client.clone()))
            .map_ok(move |(user, config, client)| {
                let out = try_stream! {
                    let mut pagination = None;
                    loop {
                        let query: Vec<_> = std::array::IntoIter::new([user.get(), first("100")]).chain(pagination.as_deref().map(after)).collect();
                        let res = request(VIDEOS, &client, &query, Get, &config).await?;
                        let res = res
                            .json::<VodData>()
                            .await?;

                        pagination = res.pagination.cursor;

                        for item in res.data {
                            // println!("{:?}", item);
                            yield item;
                        }
                        if pagination == None { break; }
                    }
                };
                out
                .boxed_local()
            });
        let out = Box::pin(out);
        Vods(out)
    }
    pub fn extract_host(coll: &mut Vec<UserId>, config: &Config) -> Option<UserId> {
        let uni = UniCase::new(&config.user_name);
        coll.iter()
            .position(|UserId { login, .. }| UniCase::new(&***login) == uni)
            .map(|host| coll.remove(host))
    }
    pub async fn from_names(
        users: impl Stream<Item = Result<String>>,
        client: Client,
        config: Arc<Config>,
    ) -> Result<Self> {
        // assert!(!users.is_empty());
        use crate::parse::IntoLowerCase;

        let len = users.size_hint().0;

        let cache: HashMap<Rc<String>, UserId> = cache_read("user_id").await?;
        let cache: HashMap<_, _> = cache.iter().map(|(k, v)| (UniCase::new(&**k), v)).collect();

        let users = users.map_ok(|mut i| {
            i.into_lowercase();
            i
        });

        let (queue, out) = users
            .try_fold(
                (Vec::with_capacity(len), Vec::with_capacity(len)),
                |(mut queue, mut out), login| {
                    future::ok({
                        if let Some(id) = cache.get(&UniCase::new(&login)).map(|i| (*i).clone()) {
                            out.push(id)
                        } else {
                            queue.push(login)
                        }
                        (queue, out)
                    })
                },
            )
            .await?;

        let out = stream::iter(queue)
            .chunks(100)
            .map(move |i| (i, client.clone(), Arc::clone(&config)))
            .map(|(batch, client, config)| async move {
                let out = request(
                    USERS,
                    &client,
                    &generate_pair("login", &batch).collect::<Vec<_>>(),
                    Get,
                    &config,
                )
                .await?
                .json::<UserIdData>()
                .await?
                .data;
                let mut cache: HashMap<Arc<String>, _> = cache_read("user_id").await?;
                cache.extend(
                    out.iter()
                        .map(|user_id| (Arc::clone(&user_id.login), user_id.clone())),
                );
                cache_write("user_id", &cache).await?;
                Result::<_>::Ok(out)
            })
            .buffer_unordered(usize::MAX)
            .map_ok(|i| stream::iter(i).map(Ok))
            .try_flatten()
            .chain(stream::iter(out).map(Result::<_>::Ok));

        // new_cache_write("user_id", &cache).await?;
        Ok(UserIds(Box::pin(out)))
    }
    pub(crate) fn to_live_streams(self, client: Client, config: Arc<Config>) -> LiveStream {
        let users = self.map_ok(|UserId { login, .. }| {
            Arc::try_unwrap(login).unwrap_or_else(|i| i.to_string())
        });
        LiveStream::from_names(users, client, config)
    }
    pub(crate) async fn from_followed(
        logins: impl Stream<Item = String>,
        client: Client,
        config: Arc<Config>,
    ) -> Result<Self> {
        let mut num = 100;
        let users =
            UserIds::from_names(logins.map(Ok), client.clone(), Arc::clone(&config)).await?;

        let out = users
            .map_ok(move |i| (i, client.clone(), Arc::clone(&config)))
            .map_ok(move |(UserId { id, .. }, client, config)| {
                let mut count = 0;
                let mut pagination = None;
                try_stream! {
                    loop {
                        let page = num.to_string();
                        let mut query = vec![from_id(&id), first(&page)];
                        if let Some(pagination) = pagination.as_deref() {
                            query.push(after(pagination));
                        }
                        let res = request(FOLLOWS, &client, &query, Get, &config)
                            .await?
                            .json::<FollowListData>()
                            .await?;

                        let total = res.total;

                        count += res.data.len();

                        for FollowItem { to_login, to_id, followed_at } in res.data {
                            yield UserId {login: to_login, id: to_id, followed_at: Some(followed_at) };
                        }
                        if count >= total {
                            break;
                        }

                        pagination = res.pagination.cursor;
                        num = (num - count).clamp(0, 100);
                    }
                }
            })
            .try_flatten();

        Ok(UserIds(Box::pin(out)))
    }
}
#[derive(Deserialize)]
pub(crate) struct StreamPlayBackAccessToken {
    value: String,
    signature: String,
}
#[derive(Deserialize)]
pub(crate) struct AccessTokenData {
    #[serde(rename = "streamPlaybackAccessToken")]
    stream_playback_access_token: StreamPlayBackAccessToken,
}
#[derive(Deserialize)]
pub(crate) struct AccessTokenResponse {
    data: AccessTokenData,
}
#[macro_export]
macro_rules! pair {
    ( $( $x:ident ),+ ) => {

        $(
        pub(crate) const fn $x(val: &str) -> (&str, &str) {
            (stringify!($x), val)
        }
        )*
    }
}
#[derive(Serialize, Debug)]
pub(crate) struct PersistedQuery {
    #[serde(rename = "sha256Hash")]
    sha256_hash: &'static str,
    version: u8,
}
#[derive(Serialize, Debug)]
pub(crate) struct Variables {
    #[serde(rename = "isLive")]
    is_live: bool,
    #[serde(rename = "isVod")]
    is_vod: bool,
    login: String,
    #[serde(rename = "playerType")]
    player_type: String,
    #[serde(rename = "vodID")]
    vod_id: &'static str,
}
#[derive(Serialize, Debug)]
pub(crate) struct Extensions {
    #[serde(rename = "persistedQuery")]
    persisted_query: PersistedQuery,
    #[serde(rename = "operationName")]
    operation_name: &'static str,
}
#[derive(Serialize, Debug)]
pub(crate) struct AccessTokenSend {
    extensions: Extensions,
    variables: Variables,
}
impl AccessTokenSend {
    fn new(name: impl Into<String>, player_type: String) -> Self {
        let name = name.into().to_lowercase();
        Self {
            extensions: Extensions {
                persisted_query: PersistedQuery {
                    sha256_hash: "0828119ded1c13477966434e15800ff57ddacf13ba1911c129dc2200705b0712",
                    version: 1,
                },
                operation_name: "PlaybackAccessToken",
            },
            variables: Variables {
                is_live: true,
                is_vod: false,
                login: name,
                player_type,
                vod_id: "",
            },
        }
    }
}
#[derive(Deserialize, Serialize, Debug)]
pub(crate) struct Authorization {
    forbidden: bool,
    reason: String,
}
#[derive(Deserialize, Serialize, Debug)]
pub(crate) struct ChanSub {
    restricted_bitrates: Vec<String>,
    view_until: u64,
}
#[derive(Deserialize, Serialize, Debug)]
pub(crate) struct Private {
    allowed_to_view: bool,
}
#[derive(Deserialize, Serialize, Debug)]
pub(crate) struct AccessToken {
    adblock: bool,
    authorization: Authorization,
    blackout_enabled: bool,
    channel_id: u64,
    channel: String,
    chansub: ChanSub,
    ci_gb: bool,
    device_id: Option<String>,
    expires: u64,
    extended_history_allowed: bool,
    game: String,
    geoblock_reason: String,
    hide_ads: bool,
    https_required: bool,
    mature: bool,
    partner: bool,
    platform: String,
    player_type: String,
    private: Private,
    privileged: bool,
    role: String,
    server_ads: bool,
    show_ads: bool,
    subscriber: bool,
    turbo: bool,
    user_id: Option<String>,
    user_ip: String,
    version: u8,
}
#[derive(Serialize, Deserialize, Debug)]
pub(crate) struct GQLAccess {
    token: String,
    signature: String,
}
async fn get_access_token(user: &str, client: &Client, player_type: String) -> Result<GQLAccess> {
    let data = AccessTokenSend::new(user, player_type);
    let res = client
        .post(GQL)
        .header("Client-ID", "kimne78kx3ncx6brgo4mv6wki5h1ko")
        .header(ACCEPT_ENCODING, "gzip")
        .header(USER_AGENT, "Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0")
        .json(&data)
        .send()
        .await?
        .error_for_status()?;
    // panic!("{}", res.text().await?);
    let res = res
        .json::<AccessTokenResponse>()
        .await?
        .data
        .stream_playback_access_token;
    // let token: AccessToken = serde_json::from_str(&res.value)?;
    let out = GQLAccess {
        token: res.value,
        signature: res.signature,
    };
    Ok(out)
}
async fn request_stream<T: Serialize + ?Sized + std::fmt::Debug>(
    node: &str,
    client: &Client,
    query: &T,
) -> Result<Response> {
    client
        .get(node)
        .header("Client-ID", "kimne78kx3ncx6brgo4mv6wki5h1ko")
        .header(ACCEPT_ENCODING, "gzip")
        .header(USER_AGENT, "Googlebot")
        .query(query)
        .send()
        .await?
        .error_for_status()
        .map_err(From::from)
}
pair!(
    allow_source,
    fast_bread,
    playlist_includes_framerate,
    sig,
    token
);

use core::time::Duration as StdDuration;
pub(crate) struct PlaylistStream {
    pub(crate) stream: Pin<Box<dyn Stream<Item = Result<PlaylistSegment>>>>,
}

impl Stream for PlaylistStream {
    type Item = Result<PlaylistSegment>;

    fn poll_next(
        mut self: Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Option<Self::Item>> {
        self.stream.poll_next_unpin(cx)
    }
}
use async_stream::stream;

pub(crate) struct PlaylistSegment {
    segment: Pin<Box<dyn Stream<Item = reqwest::Result<Bytes>>>>,
    low_latency: bool,
}

impl PlaylistSegment {
    pub(crate) fn new(
        segment: Pin<Box<dyn Stream<Item = reqwest::Result<Bytes>>>>,
        low_latency: bool,
    ) -> Self {
        Self {
            segment,
            low_latency,
        }
    }

    pub(crate) const fn is_low_latency(&self) -> bool {
        self.low_latency
    }
    pub(crate) async fn write(&mut self, writer: &mut (impl AsyncWrite + Unpin)) -> Result<()> {
        while let Some(chunk) = self.segment.next().await.transpose()? {
            writer.write_all(&chunk).await?;
        }
        Ok(())
    }
    async fn from_playlist(
        mut playlist: MediaPlaylist,
        user_name: &str,
        client: &Client,
        last_segment_uri: &mut Option<String>,
        ad_free_stream: &mut Option<PlaylistStream>,
        
    ) -> Result<Self> {
        let mut low_latency_segment_uris: Vec<_> = playlist
            .unknown_tags
            .iter_mut()
            .filter_map(|ExtTag { ref tag, rest }| {
                (tag == "X-TWITCH-PREFETCH").then(|| mem::take(rest))
            })
            .collect();

        // assert!(low_latency_segment_uris.len() < 3);

        if low_latency_segment_uris.is_empty()
            && playlist
                .unknown_tags
                .iter()
                .rev()
                .any(|ExtTag { rest, .. }| rest.contains("stitched-ad"))
        {
            eprintln!("Replacing ad stream with low-quality, ad-free stream ...");
            if ad_free_stream.is_none() {
                *ad_free_stream = {
                    let (mut playlist, _) = get_master_playlist(&user_name, client, String::from("thunderdome")).await?;
                    let url = playlist.variants.remove(0).uri;
                    let stream = PlaylistStream::playlist_segment_stream(url, user_name.to_owned(), client.clone())?;
                    Some(stream)
                }
            }
            let ad_free_stream = ad_free_stream.as_mut().unwrap();
            let stream = ad_free_stream.next().await.transpose()?.expect("expected some");
            Ok(stream)
        } else if !low_latency_segment_uris.is_empty() {
            // play both segments, in-order
            if let Some(last_segment_uri) = last_segment_uri {
                low_latency_segment_uris
                    .iter()
                    .position(|got_segment| got_segment == last_segment_uri)
                    .map(|idx| low_latency_segment_uris.remove(idx));
            }
            *last_segment_uri = low_latency_segment_uris.last().cloned();
            let stream =
                stream::iter(low_latency_segment_uris)
                    .zip(stream::iter(iter::once(client.clone()).cycle()))
                    .map(|(uri, client)| async move {
                        Ok(client.get(&uri).send().await?.bytes_stream())
                    })
                    .buffered(usize::MAX)
                    .try_flatten()
                    .err_into();

            Result::Ok(Self::new(Box::pin(stream), true))
        } else if let Some(MediaSegment { uri, .. }) = playlist.segments.last() {
            let out = client.get(uri).send().await?.bytes_stream().err_into();

            // next_segment = low_latency_segments.next().map(|(one, two)| (one.to_owned(), two));

            Result::Ok(Self::new(Box::pin(out), false))
        } else {
            todo!();
        }
    }
}

impl PlaylistStream {
    pub(crate) async fn play(&mut self, config: Arc<Config>) -> Result<()> {
        let Config {
            live_player,
            live_player_args,
            ..
        } = &*config;

        let mut player = Command::new(live_player)
            .args(live_player_args)
            .stdin(Stdio::piped())
            .spawn()?;

        let stdin = player.stdin.as_mut().unwrap();

        self.play_into(stdin).await?;

        player.wait().await?;

        Ok(())
    }
    pub(crate) async fn play_into(&mut self, writer: &mut (impl AsyncWrite + Unpin)) -> Result<()> {
        let mut time = Instant::now();
        while let Some(mut segment) = self.next().await.transpose()? {
                segment.write(writer).await?;

            if !segment.low_latency {
                tokio::time::sleep(std::time::Duration::from_secs(2) - time.elapsed()).await;
            }

            time = Instant::now();
        }
        Ok(())
    }
    async fn obtain_playlist(url: &str) -> Result<MediaPlaylist> {
        /*
            We can't actually reuse our client here because
            Amazon's playlist server will drop the handshake after inactivity.
            We will receive Err(hyper::IncompleteResponse) in that scenario.
        */

        let get = Client::new()
            .get(url)
            .header(ACCEPT_ENCODING, "gzip")
            .header(
                USER_AGENT,
                "Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0",
            )
            .send()
            .await?
            .error_for_status()?
            .bytes()
            .await?;
        match m3u8_rs::parse_playlist_res(&get) {
            Ok(Playlist::MediaPlaylist(playlist)) => Ok(playlist),
            Ok(Playlist::MasterPlaylist(playlist)) => panic!(
                "Expected media playlist, found master playlist: {:?}",
                playlist
            ),
            other => panic!("failed to parse media playlist: {:?}", other),
        }
    }
    pub(crate) fn playlist_segment_stream(url: String, user_name: String, client: Client) -> Result<PlaylistStream> {
        let out = stream! {
            let mut ad_free_stream = None;
            let mut last_segment_uri = None;
            loop {
                let playlist = Self::obtain_playlist(&url).await?;
                let segment = PlaylistSegment::from_playlist(playlist, &user_name, &client, &mut last_segment_uri, &mut ad_free_stream).await;
                yield segment.map_err(Into::into);
            }
        };
        Ok(Self {
            stream: Box::pin(out),
        })
    }
}

pub async fn get_master_playlist(user: &str, client: &Client, player_type: String) -> Result<(MasterPlaylist, String)> {
    let GQLAccess { token, signature } = get_access_token(user, client, player_type).await?;

    let out = client.get(
        &format!("{}/api/channel/hls/{}.m3u8", USHER, user.to_lowercase()),

    ).query(&[
        allow_source("true"),
        // dt("2"),
        fast_bread("true"),
        playlist_includes_framerate("true"),
        // reassignments_supported("true"),
        // supported_codecs("avc1"),
        // cdm("wv"),
        // player_version("1.4.0"),
        self::token(&token),
        sig(&signature),
    ])
    .header(ORIGIN, "https://player.twitch.tv")
    .header(REFERER, "https://player.twitch.tv")
    .send()
    .await?
    .bytes()
    .await?;

    eprintln!("{:#?}", out);

    let res = match m3u8_rs::parse_playlist_res(&out) {
        Ok(Playlist::MasterPlaylist(playlist)) => playlist,
        other => panic!("Failed to parse playlist: {:?}", other),
    };
    Ok((res, user.to_owned()))
}
#[derive(Deserialize)]
pub struct GenericIdProperties {
    pub id: String,
}
