use std::{iter, rc::Rc, sync::Arc};

use eframe::epi::App;
use egui::{CentralPanel, CtxRef, ScrollArea, Ui};
use epi::Frame;
use futures::{stream, Stream, StreamExt};
use once_cell::sync::OnceCell;
use reqwest::Client;
use tokio::runtime::Runtime;

use crate::{
    fetch::UserIds,
    parse::Args,
    streams::{LiveList, LiveStream},
    tools::{read_config, Config},
};

pub trait Render {
    fn render(
        &mut self,
        ui: &mut Ui,
        ctx: &CtxRef,
        frame: &mut Frame,
        runtime: &Runtime,
        client: &Client,
        config: &Arc<Config>,
    );
}
use TwitchPage::*;
#[derive(Default)]
pub(crate) struct RunningApp {
    pub(crate) pages: Vec<TwitchPage>,
    pub(crate) current_idx: usize,
}

impl App for RunningApp {
    fn update(&mut self, ctx: &egui::CtxRef, frame: &mut eframe::epi::Frame<'_>) {
        static RUNTIME: OnceCell<Runtime> = OnceCell::new();
        static CLIENT: OnceCell<Client> = OnceCell::new();
        static CONFIG: OnceCell<Arc<Config>> = OnceCell::new();
        CentralPanel::default().show(ctx, |ui| {
            ScrollArea::auto_sized().show(ui, |ui| {
                let runtime = RUNTIME.get_or_init(|| {
                    tokio::runtime::Builder::new_current_thread()
                        .enable_all()
                        .build()
                        .unwrap()
                });
                self.render(
                    ui,
                    ctx,
                    frame,
                    runtime,
                    CLIENT.get_or_init(|| Client::new()),
                    CONFIG.get_or_init(|| Arc::new(read_config().expect("Failed to read config"))),
                )
            })
        });
    }

    fn name(&self) -> &str {
        "TwitchRS"
    }
}

pub(crate) struct LiveData {
    list: LiveList,
    stream: LiveStream,
}

pub(crate) enum TwitchPage {
    StreamListing(LiveData),
}

impl From<LiveData> for TwitchPage {
    fn from(v: LiveData) -> Self {
        Self::StreamListing(v)
    }
}
use crate::error::Result;

impl LiveData {
    fn from_top_streams(client: Client, config: Arc<Config>) -> Self {
        Self {
            list: LiveList::new(),
            stream: LiveStream::from_top_streams(client.clone(), Arc::clone(&config))
                .filtered(config, false)
                .with_thumbnails(client),
        }
    }
    fn from_users(
        users: impl Stream<Item = Result<String>> + 'static,
        client: Client,
        config: Arc<Config>,
    ) -> Self {
        Self {
            list: LiveList::new(),
            stream: LiveStream::from_names(users, client.clone(), Arc::clone(&config))
                .filtered(config, false)
                .with_thumbnails(client),
        }
    }
    async fn from_self_followed(client: Client, config: Arc<Config>) -> Result<Self> {
        Self::from_followed(
            stream::iter(iter::once(config.user_name.to_owned())),
            client,
            config,
        )
        .await
    }
    async fn from_followed(
        logins: impl Stream<Item = String>,
        client: Client,
        config: Arc<Config>,
    ) -> Result<Self> {
        Ok(Self {
            list: LiveList::new(),
            stream: UserIds::from_followed(logins, client.clone(), Arc::clone(&config))
                .await?
                .to_live_streams(client.clone(), Arc::clone(&config))
                .filtered(config, false)
                .with_thumbnails(client),
        })
    }
}

impl Render for LiveData {
    fn render(
        &mut self,
        ui: &mut Ui,
        ctx: &CtxRef,
        frame: &mut Frame,
        runtime: &Runtime,
        client: &Client,
        config: &Arc<Config>,
    ) {
        let Self { list, stream } = self;
        if list.is_empty() {
            let get = runtime
                .block_on(stream.collect_at_most(100))
                .expect("Failed to get page of live streams");
            list.extend(get);
        }

        list.render(ui, ctx, frame, runtime, client, config);

        if ui.button("Next Page").clicked() {
            let get = runtime
                .block_on(stream.collect_at_most(100))
                .expect("Failed to get page of live streams");
            list.extend(get);
        }
    }
}

impl Render for TwitchPage {
    fn render(
        &mut self,
        ui: &mut Ui,
        ctx: &CtxRef,
        frame: &mut Frame,
        runtime: &Runtime,
        client: &Client,
        config: &Arc<Config>,
    ) {
        match self {
            StreamListing(list) => list.render(ui, ctx, frame, runtime, client, config),
        }
    }
}
impl Render for RunningApp {
    fn render(
        &mut self,
        ui: &mut Ui,
        ctx: &CtxRef,
        frame: &mut Frame,
        runtime: &Runtime,
        client: &Client,
        config: &Arc<Config>,
    ) {
        if self.pages.is_empty() {
            let get = LiveData::from_top_streams(client.clone(), Arc::clone(config)).into();
            self.pages.push(get);
        }
        self.pages[self.current_idx].render(ui, ctx, frame, runtime, client, config);
    }
}
