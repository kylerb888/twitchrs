use crate::{clips, error::Error, fetch::{self, PlaylistStream, PostOrGet, UserIds}, follows::follow_user, loader::twitch::{self, check_live_and_play}, presentation::{self, prompt, vod_chat_replay}, streams::{LiveStream, PreprocessedLiveList}, tools::{Config, OtherBlackList, TwitchSite}, vod::RunAt};
use crate::{
    fetch::LiveProperties, parse::Action::*, streams::LiveList, tools::read_config,
    FindFirstAndTake,
};
use futures::{future, stream, StreamExt, TryStreamExt};
use m3u8_rs::playlist::{MasterPlaylist, MediaPlaylist, VariantStream};
use std::{env, process::Stdio, rc::Rc, sync::Arc, time::Instant};
use std::{ops::Deref, time::Duration};
use tokio::time::sleep;
use unicase::UniCase;
const ACTION: &[&str] = &[
    "followed_vods",
    "get_playlist",
    "stream",
    "popout",
    "chat",
    "cli_chat",
    "game",
    "videos",
    "page",
    "userid",
    "last",
    "past",
    "pastdays",
    "paststreams",
    "live",
    "get_top_streams",
    "get_follows",
    "follow",
    "follownotify",
    "clip",
    "lastid",
    "vod_chat",
    "blacklist",
    "-h",
    "--help",
];

#[derive(Clone)]
pub struct OptArgs {
    pub action: Option<Action>,
    pub quality: Option<String>,
    pub force: bool,
    pub gui: bool,
}

#[derive(Clone)]
pub struct Args {
    pub list: Vec<String>,
    pub optargs: OptArgs,
    pub config: Arc<Config>,
}
use crate::tools::TwitchSite::*;
use crate::vod::Period::*;
impl Args {
    pub(crate) async fn execute(self) -> Result<()> {
        match &self.optargs.action {
            Some(FollowedVods) => self.followed_vods().await?,
            Some(Stream) => self.check_live_and_play().await?,
            Some(Popout) => self.load_popouts().await?,
            Some(Chat) => self.load_chats().await?,
            Some(CliChat) => self.cli_chat().await?,
            Some(Game) => self.list_games().await?,
            Some(Action::Videos) => self.load_vods().await?,
            Some(Page) => self.load_pages().await?,
            Some(UserId) => self.get_user_ids().await?,
            Some(Past) => self.get_past_vods().await?,
            Some(Live) => self.check_live().await?,
            Some(GetTopStreams) => self.get_top_streams().await?,
            Some(GetFollows) => self.get_follows().await?,
            Some(Follow) => self.follow_user().await?,
            Some(FollowNotify) => self.follow_notify().await?,
            Some(Clip) => self.create_clips().await?,
            Some(LastId) => self.get_last_vod_id().await?,
            Some(VodChat) => self.vod_chat().await?,
            Some(Help) => print_help(),
            Some(GetPlaylist) => self.check_live_and_play().await?,
            Some(BlackList) => self.blacklist().await?,
            None if self.list.is_empty() => self.prompt_load_streams().await?,
            None => self.check_live_and_play().await?,
        };
        Ok(())
    }

    async fn load_popouts(self) -> Result<()> {
        Popouts(self.list).open().await
    }

    async fn cli_chat(self) -> Result<()> {
        presentation::chat(self.list).await
    }

    async fn load_chats(self) -> Result<()> {
        Chats(self.list).open().await
    }

    async fn prompt_load_streams(self) -> Result<()> {

        let client = reqwest::Client::new();
        
        let mut res = LiveList::from_followed(client.clone(), &self).await?;
        let num = prompt::multi_num("watch the live stream of a followed streamer")?;
        let query: Vec<String> = num
            .into_iter()
            .map(|item| res.swap_remove(item).user_login)
            .collect();
        twitch::check_live_and_play(query, &self.optargs, &self.config, &client).await?;
        Ok(())
    }

    async fn blacklist(mut self) -> Result<()> {
        let declaration =
            self.list
                .find_first_and_take(&["game", "title", "partial_game", "user", "print"]);
        let matched = match declaration.as_deref() {
            Some("game") => OtherBlackList::Game,
            Some("title") => OtherBlackList::Title,
            Some("partial_game") => OtherBlackList::PartialBlackListGame,
            Some("user") => OtherBlackList::User,
            _ => OtherBlackList::Print,
        };
        if matched == OtherBlackList::Print {
            println!("{}", self.config)
        } else {
            Arc::make_mut(&mut self.config).extend(matched, self.list)?;
        }
        Ok(())
    }

    async fn check_live_and_play(self) -> Result<()> {
        let Self { 
            list, 
            config, 
            optargs: OptArgs { quality, .. },
            .. 
        } = self;

        let client = reqwest::Client::new();

        if self.optargs.force {
            return play_streams(list, client, config, quality.as_deref()).await;
        }

        let list_length = list.len();

        let (browser, video_player): (Vec<_>, Vec<_>) = LiveProperties::from_names(list, Arc::clone(&config))
            .await?
            .into_iter()
            .filter_map(|LiveProperties { user_name, live_status, .. }| live_status.is_some().then(|| (user_name, live_status.unwrap())))
            .fold((Vec::new(), Vec::with_capacity(list_length)), |(mut sites, mut video_player), (name, StreamItem { open_in_site })| {
                if open_in_site {
                    sites.push(format!("https://www.twitch.tv/{}", name));
                } else {
                     video_player.push(name);
                 }
                 (sites, video_player)
            });

        Sites(browser).open().await?;

        

    play_streams(video_player, client, config, quality.as_deref()).await?;
    Ok(())
    }

    async fn vod_chat(mut self) -> Result<()> {
        let first = &*self.list.remove(0);
        let untimed = self.list.find_first_and_take(&["--untimed"]).is_some();
        match first {
            "time" => {
                vod_chat_replay(
                    &self.list.remove(0),
                    &RunAt::OffsetSecs(self.list.remove(0)),
                    &self.config,
                    untimed,
                )
                .await
            }
            _ => vod_chat_replay(&first, &RunAt::Cursor(None), &self.config, untimed).await,
        }
    }

    async fn get_last_vod_id(self) -> Result<()> {
        let client = reqwest::Client::new();
        let vod = UserIds::from_names(
            stream::iter(self.list).map(Ok),
            client.clone(),
            Arc::clone(&self.config),
        )
        .await?
        .get_vods(client, self.config)
        .try_flatten()
        .try_next()
        .await?;
        if let Some(vod) = vod {
            println!("{}", vod.id);
        } else {
            eprintln!("There are no vods.");
        }
        Ok(())
    }

    async fn create_clips(self) -> Result<()> {
        let res = clips::Clips::create(self.list, reqwest::Client::new(), self.config).await?;
        println!("{}", res);
        Clips(res.0).open().await?;
        Ok(())
    }

    async fn follow_notify(self) -> Result<()> {
        let args = &self;
        const MINUTE: Duration = Duration::from_secs(60);
        let mut old_users: Vec<String> = Vec::new();
        let mut first_run = true;
        let client = reqwest::Client::new();
        loop {
            let new_list = LiveList::from_followed(client.clone(), args).await?;
            if !first_run {
                new_list.update(&old_users);
            } else {
                first_run = false;
            }
            let current_users: Vec<String> = new_list
                .into_iter()
                .map(|PreprocessedLiveList { user_name, .. }| user_name)
                .collect();
            sleep(MINUTE).await;
            old_users = current_users;
        }
    }

    async fn follow_user(mut self) -> Result<()> {
        let verb = self.list.find_first_and_take(&["add", "remove"]).expect("add/remove verb not found");
        let verb = match &*verb {
            "add" => PostOrGet::Post,
            "remove" => PostOrGet::Delete,
            _ => unreachable!(),
        };
        follow_user(self.list, reqwest::Client::new(), self.config, verb).await
    }

    async fn get_follows(self) -> Result<()> {
        let client = reqwest::Client::new();
        UserIds::from_followed(stream::iter(self.list), client, self.config)
            .await?
            .try_for_each(|i| future::ok(print!("{} ", i)))
            .await?;
        Ok(())
    }

    async fn get_top_streams(self) -> Result<()> {
        let client = reqwest::Client::new();
        let res = LiveStream::from_top_streams(client, Arc::clone(&self.config))
            .filtered(self.config, self.optargs.force)
            .collect_at_most(100)
            .await?
            .merged();
        print!("{}", res);
        Ok(())
    }

    async fn check_live(self) -> Result<()> {
        let client = reqwest::Client::new();
        let res = LiveStream::from_names(stream::iter(self.list).map(Ok), client, self.config)
            .into_live_list()
            .await?
            .merged();
        print!("{}", res);
        Ok(())
    }

    async fn get_past_vods(mut self) -> Result<()> {
        let find = self.list.find_first_and_take(&["days", "streams"]);
        let remove = self.list.remove(0);

        let period = match find.as_deref() {
            Some("days") => Time(remove.parse::<i64>()?),
            Some("streams") => Num(remove.parse::<usize>()?),
            _ => Num(7),
        };
        let client = reqwest::Client::new();
        UserIds::from_names(
            stream::iter(self.list).map(Ok),
            client.clone(),
            Arc::clone(&self.config),
        )
        .await?
        .get_vods(client, Arc::clone(&self.config))
        .limit(period)
        .show(&self.config)
        .await
    }

    async fn get_user_ids(self) -> Result<()> {
        UserIds::from_names(
            stream::iter(self.list).map(Ok),
            reqwest::Client::new(),
            self.config,
        )
        .await?
        .try_for_each(|i| future::ok(print!("{}", i)))
        .await?;
        Ok(())
    }

    async fn load_pages(self) -> Result<()> {
        Sites(self.list).open().await
    }

    async fn load_vods(self) -> Result<()> {
        TwitchSite::Videos(self.list).open().await
    }

    async fn list_games(self) -> Result<()> {
        let client = reqwest::Client::new();
        let mut res = LiveList::from_games(
            stream::iter(self.list),
            client.clone(),
            Arc::clone(&self.config),
        )
        .await?;
        print!("{}", res);
        let idc = prompt::multi_num("watch one of these streams")?;

        let res = idc
            .into_iter()
            .map(|idx| res.swap_remove(idx).user_login)
            .collect();

        check_live_and_play(res, &self.optargs, &self.config, &client).await?;

        Ok(())
    }

    async fn followed_vods(self) -> Result<()> {
        let client = reqwest::Client::new();
        UserIds::from_followed(
            stream::iter(Some(self.config.user_name.to_owned())),
            client.clone(),
            Arc::clone(&self.config),
        )
        .await?
        .get_vods(client, Arc::clone(&self.config))
        .show(&self.config)
        .await
    }
}
async fn play_streams(video_player: Vec<String>, client: reqwest::Client, config: Arc<Config>, resolution: Option<&str>) -> Result<()> {
    let mut stream = stream::iter(video_player)
        .zip(stream::iter(Some(client.clone())).cycle())
        .map(|(item, client)| async move { fetch::get_master_playlist(&item, &client, String::from("embed")).await })
        .buffer_unordered(usize::MAX)
        .map_ok(|(playlist, name)| (playlist, name, client.clone()))
        .and_then(|(MasterPlaylist { mut variants, .. }, name, client)| future::ready((|| {

                let url = if let Some(target) = resolution {
                    variants.iter().find_map(|VariantStream { resolution, uri, .. }| {
                        (resolution.as_ref()?.rsplit('x').next()? == target.strip_suffix('p')?).then(|| uri)
            
                    }).ok_or_else(|| Error::MissingResolution(
                        variants
                            .iter()
                            .filter_map(|VariantStream { resolution, .. }| {
                                resolution.as_ref()?.rsplit('x').next()?.parse::<u32>().ok()
                            })
                            .collect()
                        )
                    )?.to_owned()
                } else {
                    variants.swap_remove(0).uri
                };
                PlaylistStream::playlist_segment_stream(
                    url,
                    name,
                    client,

                )
            })()))
        .map_ok(|i| (i, Arc::clone(&config)))
        .map_ok(|(mut item, config)| async move { item.play(config).await })
        .try_buffer_unordered(usize::MAX);
    let res = stream
        .by_ref()
        .filter(|i| future::ready(i.is_err()))
        .next()
        .await
        .unwrap_or(Ok(()));
    stream.for_each(|_| future::ready(())).await;
    res?;
    Ok(())
}

fn print_help() {
    println!(
        concat!(
            "Twitch wrapper that plays a stream in a video player of your choice using streamlink and/or displays twitch chat window of a user in your web browser. Leverages Twitch API V6.\n",
            "   Usage: twitchrs  [type] *[channel(s)]\n",
            "   or: twitchrs [channel(s)]\n",
            "   or: twitchrs [void]\n",
            '\n',
            "[type]\n",
            "   stream        : Live stream of user.\n",
            "   chat          : Popout chat of user.\n",
            "   game          : Game category on twitch.\n",
            "   videos        : Open up the videos page for users.\n",
            "   page          : Load up the page of provided users.\n",
            "   userid        : Gets the user IDs of provided users.\n",
            "   last          : Play streamer's last VOD.\n",
            "   past          : Grabs a user's past streams.\n",
            "   pastdays      : Grabs a user's past streams within a certain amount of days provided.\n",
            "                       [pastdays|paststreams] [num] [channels]\n",
            "   paststreams   : Grabs a user's past streams with a stream amount provided.\n",
            "   live          : Returns the name of the user if live.\n",
            "   clip          : Clip a user's currently running stream.\n",
            "   lastid        : print last VOD ID in console\n",
            '\n',
            "[void]           : These functions do not take any input.\n",
            "   watchfollowed : will update every minute and display a list of your followed users that are live.\n",
            "   follownotify  : will display a list of your followed users that are live.\n",
            '\n',
            "This ferrous ore has super oxidative powers.\n")
        );
}

impl<T> From<&T> for Action
where
    T: AsRef<str> + ?Sized,
{
    fn from(action: &T) -> Self {
        match action.as_ref() {
            "chat" => Chat,
            "cli_chat" => CliChat,
            "clip" => Clip,
            "follow" => Follow,
            "follownotify" => FollowNotify,
            "game" => Game,
            "followed_vods" => FollowedVods,
            "get_follows" => GetFollows,
            "get_playlist" => GetPlaylist,
            "get_top_streams" => GetTopStreams,
            "lastid" => LastId,
            "live" => Live,
            "page" => Page,
            "past" => Past,
            "popout" => Popout,
            "stream" => Stream,
            "userid" => UserId,
            "videos" => Self::Videos,
            "vod_chat" => VodChat,
            "blacklist" => BlackList,
            _ => Help,
        }
    }
}
#[derive(Clone)]
pub enum Action {
    Chat,
    CliChat,
    Clip,
    Follow,
    FollowNotify,
    FollowedVods,
    Game,
    GetFollows,
    GetPlaylist,
    GetTopStreams,
    Help,
    LastId,
    Live,
    Page,
    Past,
    Popout,
    Stream,
    UserId,
    Videos,
    VodChat,
    BlackList,
}
fn is_resolution(value: &str) -> bool {
    if let Some(res) = value.strip_suffix('p') {
        res.bytes().all(|c| c.is_ascii_digit())
    } else {
        matches!(value, "best" | "worst" | "audio_only")
    }
}
pub fn parse_args() -> Result<Args> {
    let mut list: Vec<String> = env::args().skip(1).collect();
    let quality = list
        .iter()
        .position(|q| is_resolution(q))
        .map(|q| list.remove(q));
    let (force, action, config, gui) = (
        list.find_first_and_take(&["--force"]).is_some(),
        list.find_first_and_take(ACTION).map(|a| (&*a).into()),
        read_config().map(Arc::new)?,
        list.find_first_and_take(&["gui"]).is_some(),
    );

    Ok(Args {
        list,
        config,
        optargs: OptArgs {
            action,
            quality,
            force,
            gui,
        },
    })
}

/*pub struct LiveHint {
    pub live: bool,
    pub popout: bool,
}
*/
#[derive(Debug)]
pub struct StreamItem {
    pub open_in_site: bool,
}
use crate::error::Result;

pub(crate) trait IntoLowerCase {
    fn into_lowercase(&mut self);
}

impl IntoLowerCase for String {
    fn into_lowercase(&mut self) {
        let bytes = unsafe { self.as_bytes_mut() };
        for byte in bytes {
            if (65..=90).contains(byte) {
                *byte += 32;
            }
        }
    }
}
