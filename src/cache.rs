use serde::{de::DeserializeOwned, Serialize};
use tokio::{
    fs::File,
    io::{AsyncReadExt, AsyncWriteExt},
};

// Perhaps it may be better to serialize the cached entries JSON?
use crate::error::Error::*;
use crate::error::Result;
use std::{
    collections::HashMap,
    hash::Hash,
    path::{Path, PathBuf},
};
pub(crate) async fn cache_write<K: Serialize + Eq + Hash, V: Serialize>(
    reqtype: impl AsRef<Path>,
    value: &HashMap<K, V>,
) -> Result<()> {
    let cache_dir = get_cache_dir(reqtype)?;
    let binary = bincode::serialize(value)?;
    let mut file = File::create(cache_dir).await?;
    file.write(&binary).await?;

    Ok(())
}

pub(crate) async fn cache_read<'de, D>(reqtype: impl AsRef<Path>) -> Result<D>
where
    D: DeserializeOwned + Default,
{
    let cache_dir = get_cache_dir(reqtype)?;
    if !cache_dir.exists() {
        return Ok(D::default());
    }
    let mut file = File::open(cache_dir).await?;
    let mut data = Vec::new();
    file.read_to_end(&mut data).await?;
    Ok(bincode::deserialize::<D>(&data).unwrap_or_else(|_| D::default()))
}

pub fn get_cache_dir(node: impl AsRef<Path>) -> Result<PathBuf> {
    let mut cache_dir = dirs::cache_dir().ok_or(NoCacheDir)?;
    cache_dir.reserve(2);
    cache_dir.push("twitchrs");
    cache_dir.push(node);
    Ok(cache_dir)
}
