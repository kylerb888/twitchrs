use crate::pair;

pair!(from_id, to_id, broadcaster_id, first, language, after);

pub const CLIPS: &str = "https://api.twitch.tv/helix/clips";
pub const USHER: &str = "https://usher.ttvnw.net";
pub const GQL: &str = "https://gql.twitch.tv/gql";
pub const FOLLOWS: &str = "https://api.twitch.tv/helix/users/follows";
pub const GAMES: &str = "https://api.twitch.tv/helix/games";
pub const STREAMS: &str = "https://api.twitch.tv/helix/streams";
pub const USERS: &str = "https://api.twitch.tv/helix/users";
pub const VIDEOS: &str = "https://api.twitch.tv/helix/videos";
