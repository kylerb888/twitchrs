use std::fmt::Display;
use std::*;
impl std::error::Error for Error {}
#[derive(Debug)]
pub enum Error {
    BadExit(i32),
    MissingResolution(Vec<u32>),
    MessageHasNoPrefix,
    MessageHasNoUserName,
    NoConfigDir,
    NoCacheDir,
    LoginNotFound,
}
pub(crate) type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;
use Error::*;
impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            MissingResolution(resolutions) => {
                f.write_str("The supplied resolution doesn't exist on the playlist.\n\n  Available resolutions:\n")?;
                for resolution in resolutions {
                    writeln!(f, "    {}p", resolution)?
                }
                
                Ok(())
            },
            NoCacheDir => f.write_str("no cache dir"),
            BadExit(exit) => write!(f, "command failed with exit code: {}", exit),
            MessageHasNoUserName => f.write_str("message has no username"),
            MessageHasNoPrefix => f.write_str("message in chat has no prefix"),
            NoConfigDir => f.write_str("Failed to resolve login directory"),
            LoginNotFound => f.write_str("Failed to find login"),
        }
    }
}
