use std::process::Command;
use std::*;
pub mod twitch {
    use crate::error::Result;
    use futures::{StreamExt, TryStreamExt, stream};
            use reqwest::Client;
    use tokio::process::Command;

    use crate::{error::Error, fetch::{LiveProperties, PlaylistStream, get_master_playlist}, tools::TwitchSite::Sites};
            use crate::{parse::OptArgs, tools::Config};
    use std::{*, sync::Arc};
        // use crate::fetch::twitch_api;
    pub const TWITCH_BASE_URL: &str = "https://www.twitch.tv";
    pub(crate) async fn loadstream(args: Vec<String>, config: &Arc<Config>, client: &Client) -> Result<()> {
        let mut stream = stream::iter(args)
            .zip(stream::iter(Some(client.clone())).cycle())
            .map(|(arg, client)| async move {
                
                get_master_playlist(&arg, &client, String::from("embed")).await 
            })
            .buffer_unordered(usize::MAX)
            .map_ok(|(playlist, name)| (playlist, name, client.clone()))
            .and_then(|(mut playlist, name, client)| {
                future::ready(
                    PlaylistStream::playlist_segment_stream(playlist.variants.remove(0).uri, name, client)
                )
            }).map_ok(|i| (i, Arc::clone(config)))
            .map_ok(|(mut i, config)| async move { i.play(config).await })
            .try_buffer_unordered(usize::MAX);

        let res = stream.next().await.unwrap_or(Ok(()));

        stream.for_each(|_| future::ready(())).await;
        res
    }
    pub(crate) async fn load_vod(name: &str, config: &Config) -> Result<()> {
        let status = Command::new(&config.vod_player)
            .args(&config.vod_player_args)
            .arg(format!("https://twitch.tv/videos/{}", name))
            .status()
            .await?;
        if status.success() {
            Ok(())
        } else {
            Err(Error::BadExit(status.code().unwrap_or(0)).into())
        }
    }
    pub(crate) async fn check_live_and_play(
        args: Vec<String>,
        optargs: &OptArgs,
        config: &Arc<Config>,
        client: &Client,
    ) -> Result<()> {
        // let res = twitch_api::get_live_status(&arg, &reqwest::blocking::Client::new());
        if optargs.force {
            return loadstream(args, config, client).await.map_err(From::from);
        }
        let res = LiveProperties::from_names(args, Arc::clone(&config)).await?;
        let (site, player): (Vec<LiveProperties>, Vec<LiveProperties>) = res
            .into_iter()
            .filter(|LiveProperties { live_status, .. }| live_status.is_some())
            .partition(|LiveProperties { live_status, .. }| {
                live_status.as_ref().unwrap().open_in_site
            });
        let site: Vec<String> = site
            .into_iter()
            .map(|LiveProperties { user_name, .. }| user_name)
            .collect();
        let player: Vec<String> = player
            .into_iter()
            .map(|LiveProperties { user_name, .. }| user_name)
            .collect();
        if !player.is_empty() {
            loadstream(player, config, client).await?;
        }
        if !site.is_empty() {
            Sites(site).open().await?;
        }
        Ok(())
    }
}
pub fn notify(name: &str) {
    Command::new("notify-send")
        .arg(name)
        .output()
        .expect("Failed to spawn notification: Do you have libnotify installed?");
}
pub fn clear() -> Result<(), io::Error> {
    Command::new("clear").status().map(drop)
}
